# dragondrop

drag and drop image files into jupyter notebook

## install

```
pip install dragondrop Pillow
```

## import and example in jupyter notebook 

```
In [1]: 
from PIL import Image
from io import BytesIO
import base64
from dragondrop import dragondrop

In [2]: dragondrop('im')

# drag and drop image file

In [3]: im.size

In [4]: im

# see image
```

